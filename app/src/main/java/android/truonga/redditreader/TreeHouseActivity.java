package android.truonga.redditreader;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;
import java.util.ArrayList;

public class TreeHouseActivity extends SingleFragmentActivity implements ActivityCallback {

    private ArrayList<RedditPost> redditPosts;
    MediaPlayer mediaPlayer;

    @LayoutRes
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_masterdetail;
    }

    @Override
    protected Fragment createFragment() {
        mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.beep);
        return new RedditListFragment();
    }



    @Override
    public void onPostSelected(Uri redditPostUri) {
        if (findViewById(R.id.detail_fragment_container) == null) {
            Intent intent = new Intent(getApplicationContext(), RedditWebActivity.class);
            intent.setData(redditPostUri);
            startActivity(intent);
            mediaPlayer.start();
        }
        else{
            Fragment detailFragment = RedditWebFragment.newfragment(redditPostUri.toString());
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.detail_fragment_container, detailFragment)
                    .commit();
        }
    }
}