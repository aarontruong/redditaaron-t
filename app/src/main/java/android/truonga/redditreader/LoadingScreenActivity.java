package android.truonga.redditreader;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class LoadingScreenActivity extends AppCompatActivity{
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        setContentView(R.layout.loadscreen);

        final Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent("android.intent.action.Reddit");
                startActivity(intent);
            }
        });

        final Button button2 = (Button) findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent("android.intent.action.TreeHouse");
                startActivity(intent);
            }
        });
    }
}

