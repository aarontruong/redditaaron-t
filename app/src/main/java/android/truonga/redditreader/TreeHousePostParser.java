package android.truonga.redditreader;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class TreeHousePostParser {

    private static TreeHousePostParser parser;

    public ArrayList<RedditPost> redditPosts;

    public static TreeHousePostParser getInstance() {

        if(parser == null) {
            parser = new TreeHousePostParser();
        }
        return parser;
    }

    public JSONObject parseInputStream(InputStream inputStream) {
        BufferedReader streamReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        JSONObject jsonObject = null;

        String currentLine;

        try {
            while ((currentLine = streamReader.readLine()) != null) {
                stringBuilder.append(currentLine);
            }
            JSONTokener jsonTokener = new JSONTokener(stringBuilder.toString());
            jsonObject = new JSONObject(jsonTokener);
        }
        catch(IOException error) {
            Log.e("TreeHousePostParser", "IOException (parseInputStream):" + error);
        }
        catch(JSONException error) {
            Log.e("TreeHouseParser", "JSONException (parseInputStream):" + error);
        }

        return jsonObject;
    }

    public void readRedditFeed(JSONObject jsonObject) {
        redditPosts.clear();

        try {
            JSONArray postData = jsonObject.getJSONArray("posts");

            for(int index = 0; index < postData.length(); index++) {
                JSONObject post = postData.getJSONObject(index);

                String title = post.getString("title");
                String url = post.getString("url");

                RedditPost redditPost = new RedditPost(title, url);
                redditPosts.add(redditPost);
            }
        }
        catch(JSONException error) {
            Log.e("RedditPostParser", "JSONException (readRedditFeed):" + error);
        }
    }
}

