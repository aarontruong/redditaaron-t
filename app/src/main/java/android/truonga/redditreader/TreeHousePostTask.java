package android.truonga.redditreader;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

    public class TreeHousePostTask extends AsyncTask<TreeHouseListFragment, Void, JSONObject> {
        private TreeHouseListFragment treeHouseListFragment;

        @Override
        protected JSONObject doInBackground(TreeHouseListFragment... treeHouseListFragments) {
            JSONObject jsonObject = null;
            treeHouseListFragment = treeHouseListFragments[0];

            try {
                URL redditFeedUrl = new URL("http://blog.teamtreehouse.com/api/get_recent_summary/?count=20");

                HttpURLConnection httpConnection = (HttpURLConnection)redditFeedUrl.openConnection();
                httpConnection.connect();

                int statusCode = httpConnection.getResponseCode();

                if(statusCode == HttpURLConnection.HTTP_OK) {
                    jsonObject = TreeHousePostParser.getInstance().parseInputStream(httpConnection.getInputStream());
                }
            }
            catch (MalformedURLException error) {
                Log.e("TreeHousePostTask", "MalformedURLException (doInBackground): " + error);
            }
            catch(IOException error) {
                Log.e("TreeHousePostTask", "IOException (doInBackground): " + error);
            }

            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {super.onPostExecute(jsonObject);
            TreeHousePostParser.getInstance().readRedditFeed(jsonObject);
            treeHouseListFragment.updateUserInterface();
        }
    }
